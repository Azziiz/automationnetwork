*** Settings ***
Library    SeleniumLibrary



*** Variables ***

${browser}  chrome
${url}     https://web.luceor.com/#/login



*** Test Cases ***
LoginTest
    open browser    ${url}  ${browser}
    loginToApplication
    clickNetwork
    addNetwork


*** Keywords ***
loginToApplication
     wait until element is visible    id:username
     wait until element is visible    id:password
     set selenium speed    1 seconds   # Adjust the delay as needed
     input text    id:username    CloudAdmin
     input text    id:password    Luceor2023
     click element    xpath://*[@id="root"]/main/div[2]/div/form/button

clickNetwork

      Wait Until Element Is Visible   xpath=//*[@id="root"]/div[1]/nav/div/div/div/ul/a[4]    5
      Click Element    xpath=//*[@id="root"]/div[1]/nav/div/div/div/ul/a[4]

addNetwork
       click element    xpath://*[@id="root"]/div[1]/main/div[2]/div/div/div/div/div/div/div[1]/div[4]/div/div/span[2]/div/button
       input text       id:tel     aziztestnetwork
       click element    xpath://*[@id="port"]
       click element    xpath://*[@id="port"]/option[2]
       click element    xpath://*[@id="MTSAPIPORT"]
       click element    xpath://*[@id="MTSAPIPORT"]/option[2]
       click element    xpath:/html/body/div[5]/div/div/div[2]/form/div[4]/div/div/button



